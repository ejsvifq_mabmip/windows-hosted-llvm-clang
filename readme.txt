This is a canadian LLVM/clang toolchain running on windows. Built by the fast_io library's author

Unix Timestamp:1675694484.677746315
UTC:2023-02-06T14:41:24.677746315Z

fast_io:
https://github.com/cppfastio/fast_io

build	:	x86_64-unknown-linux-gnu
host	:	x86_64-unknown-windows-gnu

clang++ -v
clang version 17.0.0 (https://github.com/llvm/llvm-project.git 6da0184bbc415a1c48995d8b779f476440a094ec)
Target: x86_64-unknown-windows-gnu
Thread model: posix
InstalledDir: D:/toolchains/llvm/x86_64-windows-gnu/x86_64-windows-gnu/bin
